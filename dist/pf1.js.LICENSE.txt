/*! exports provided: AbilityTemplate */

/*! exports provided: ActorPF */

/*! exports provided: ActorRestDialog */

/*! exports provided: ActorSheetFlags */

/*! exports provided: ActorSheetPF */

/*! exports provided: ActorSheetPFCharacter */

/*! exports provided: ActorSheetPFNPC */

/*! exports provided: ActorSheetPFNPCLite */

/*! exports provided: ActorSheetPFNPCLoot */

/*! exports provided: ActorTraitSelector */

/*! exports provided: ChatAttack */

/*! exports provided: ChatMessagePF */

/*! exports provided: CompendiumBrowser */

/*! exports provided: CompendiumDirectoryPF */

/*! exports provided: DicePF, _preProcessDiceFormula */

/*! exports provided: EntrySelector */

/*! exports provided: HealthConfig */

/*! exports provided: ItemPF */

/*! exports provided: ItemSheetPF */

/*! exports provided: LinkOptions */

/*! exports provided: LinksApp */

/*! exports provided: PF1 */

/*! exports provided: PatchCore */

/*! exports provided: TokenConfigPF */

/*! exports provided: TokenQuickActions */

/*! exports provided: _getInitiativeFormula, _rollInitiative, addChatMessageContextOptions */

/*! exports provided: createCustomChatMessage, hideRollInfo, hideGMSensitiveInfo, addChatCardTitleGradient */

/*! exports provided: createTag, alterRoll, createTabs, unpackVersion, isMinimumCoreVersion, degtorad, radtodeg, linkData, getItemOwner, CR, sizeDie, sizeRoll, getActorFromId */

/*! exports provided: measureDistances, measureDistance */

/*! exports provided: migrateWorld, migrateCompendium, migrateActorData, migrateItemData, migrateSceneData */

/*! exports provided: preloadHandlebarsTemplates */

/*! exports provided: registerSystemSettings */

/*! no exports provided */

/*! no static exports found */

/*!****************!*\
  !*** ./pf1.js ***!
  \****************/

/*!***********************!*\
  !*** ./module/lib.js ***!
  \***********************/

/*!************************!*\
  !*** ./module/chat.js ***!
  \************************/

/*!************************!*\
  !*** ./module/dice.js ***!
  \************************/

/*!**************************!*\
  !*** ./module/canvas.js ***!
  \**************************/

/*!**************************!*\
  !*** ./module/combat.js ***!
  \**************************/

/*!**************************!*\
  !*** ./module/config.js ***!
  \**************************/

/*!***************************!*\
  !*** ./module/measure.js ***!
  \***************************/

/*!****************************!*\
  !*** ./module/settings.js ***!
  \****************************/

/*!*****************************!*\
  !*** ./module/cytoscape.js ***!
  \*****************************/

/*!*****************************!*\
  !*** ./module/migration.js ***!
  \*****************************/

/*!*****************************!*\
  !*** ./module/templates.js ***!
  \*****************************/

/*!******************************!*\
  !*** ./module/apps/links.js ***!
  \******************************/

/*!******************************!*\
  !*** ./module/patch-core.js ***!
  \******************************/

/*!*******************************!*\
  !*** ./module/item/entity.js ***!
  \*******************************/

/*!********************************!*\
  !*** ./module/actor/entity.js ***!
  \********************************/

/*!********************************!*\
  !*** ./module/token-config.js ***!
  \********************************/

/*!*********************************!*\
  !*** ./module/config/health.js ***!
  \*********************************/

/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/

/*!***********************************!*\
  !*** ./module/apps/actor-rest.js ***!
  \***********************************/

/*!************************************!*\
  !*** ./module/actor/sheets/npc.js ***!
  \************************************/

/*!************************************!*\
  !*** ./module/apps/actor-flags.js ***!
  \************************************/

/*!************************************!*\
  !*** ./module/item/sheets/base.js ***!
  \************************************/

/*!************************************!*\
  !*** ./module/low-light-vision.js ***!
  \************************************/

/*!************************************!*\
  !*** ./module/misc/chat-attack.js ***!
  \************************************/

/*!************************************!*\
  !*** ./node_modules/heap/index.js ***!
  \************************************/

/*!*************************************!*\
  !*** ./module/actor/sheets/base.js ***!
  \*************************************/

/*!*************************************!*\
  !*** ./node_modules/color/index.js ***!
  \*************************************/

/*!**************************************!*\
  !*** ./module/sidebar/compendium.js ***!
  \**************************************/

/*!***************************************!*\
  !*** ./module/apps/entry-selector.js ***!
  \***************************************/

/*!***************************************!*\
  !*** ./module/apps/trait-selector.js ***!
  \***************************************/

/*!***************************************!*\
  !*** ./module/token-quick-actions.js ***!
  \***************************************/

/*!***************************************!*\
  !*** ./node_modules/heap/lib/heap.js ***!
  \***************************************/

/*!****************************************!*\
  !*** ./module/sidebar/chat-message.js ***!
  \****************************************/

/*!*****************************************!*\
  !*** ./module/actor/sheets/npc-lite.js ***!
  \*****************************************/

/*!*****************************************!*\
  !*** ./module/actor/sheets/npc-loot.js ***!
  \*****************************************/

/*!*****************************************!*\
  !*** ./module/pixi/ability-template.js ***!
  \*****************************************/

/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/

/*!******************************************!*\
  !*** ./module/actor/sheets/character.js ***!
  \******************************************/

/*!******************************************!*\
  !*** ./node_modules/color-name/index.js ***!
  \******************************************/

/*!*******************************************!*\
  !*** ./module/apps/compendium-browser.js ***!
  \*******************************************/

/*!*******************************************!*\
  !*** ./module/apps/links/link-options.js ***!
  \*******************************************/

/*!********************************************!*\
  !*** ./node_modules/color-string/index.js ***!
  \********************************************/

/*!*********************************************!*\
  !*** ./node_modules/color-convert/index.js ***!
  \*********************************************/

/*!*********************************************!*\
  !*** ./node_modules/color-convert/route.js ***!
  \*********************************************/

/*!**********************************************!*\
  !*** ./node_modules/lodash.memoize/index.js ***!
  \**********************************************/

/*!**********************************************!*\
  !*** ./node_modules/simple-swizzle/index.js ***!
  \**********************************************/

/*!***********************************************!*\
  !*** ./node_modules/lodash.debounce/index.js ***!
  \***********************************************/

/*!***********************************************!*\
  !*** ./node_modules/lodash.throttle/index.js ***!
  \***********************************************/

/*!************************************************!*\
  !*** ./node_modules/timers-browserify/main.js ***!
  \************************************************/

/*!***************************************************!*\
  !*** ./node_modules/color-convert/conversions.js ***!
  \***************************************************/

/*!***************************************************!*\
  !*** ./node_modules/setimmediate/setImmediate.js ***!
  \***************************************************/

/*!******************************************************!*\
  !*** ./node_modules/cytoscape/dist/cytoscape.cjs.js ***!
  \******************************************************/

/*!*************************************************************!*\
  !*** ./node_modules/cytoscape-cxtmenu/cytoscape-cxtmenu.js ***!
  \*************************************************************/

/*!*********************************************************************!*\
  !*** ./node_modules/cytoscape-edgehandles/cytoscape-edgehandles.js ***!
  \*********************************************************************/

/*!***********************************************************************!*\
  !*** ./node_modules/simple-swizzle/node_modules/is-arrayish/index.js ***!
  \***********************************************************************/
